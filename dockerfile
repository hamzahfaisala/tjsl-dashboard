### STAGE 1: Build ###
FROM node:16 as build
RUN mkdir /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --force
COPY . /app
RUN npm run build

### STAGE 2: NGINX ###
FROM nginxinc/nginx-unprivileged:stable-alpine
COPY --from=build /app/dist /usr/share/nginx/html

USER 1001

CMD ["nginx", "-g", "daemon off;"]
