import { createApp } from "vue";
import { createWebHistory, createRouter } from "vue-router";

// styles

import "@fortawesome/fontawesome-free/css/all.min.css";
import "@/assets/styles/tailwind.css";
import "flowbite";
import "tw-elements";

// mouting point for the whole app

import App from "@/App.vue";

// layouts

import Admin from "@/layouts/Admin.vue";
import Auth from "@/layouts/Auth.vue";
import Dashboard from "@/layouts/Dashboard.vue";

// views for Admin layout

import PkPenyaluran from "@/views/admin/PkPenyaluran.vue";
import Tables from "@/views/admin/Tables.vue";
import Maps from "@/views/admin/Maps.vue";
import Data from "@/views/admin/sdgs/Data.vue";
import tpb8 from "@/views/admin/sdgs/TPB8.vue";

// views for Auth layout

import Login from "@/views/auth/Login.vue";
import Register from "@/views/auth/Register.vue";

//views for Dashboard layout
import DashboardData from "@/views/dashboard";
import DashboardData1 from "@/views/dashboard/Dashboard1";
import DashboardData2 from "@/views/dashboard/Dashboard2";

// views without layouts

import Landing from "@/views/Landing.vue";
import Profile from "@/views/Profile.vue";
import Index from "@/views/Index.vue";

// routes

const routes = [
  {
    path: "/admin",
    redirect: "/admin/dashboard",
    component: Admin,
    children: [
      {
        path: "/admin/salur",
        component: PkPenyaluran,
      },
      {
        path: "/admin/tables",
        component: Tables,
      },
      {
        path: "/admin/maps",
        component: Maps,
      },
      {
        path: "/admin/sdgs/data",
        component: Data,
      },
      {
        path: "/admin/sdgs/tpb8",
        component: tpb8,
      },
    ],
  },
  {
    path: "/dashboard",
    redirect: "/dashboard",
    component: Dashboard,
    children: [
      {
        path: "/dashboard",
        component: DashboardData,
      },
      {
        path: "/dashboard1",
        component: DashboardData1,
      },
      {
        path: "/dashboard2",
        component: DashboardData2,
      },
    ],
  },
  {
    path: "/auth",
    redirect: "/auth/login",
    component: Auth,
    children: [
      {
        path: "/auth/login",
        component: Login,
      },
      {
        path: "/auth/register",
        component: Register,
      },
    ],
  },
  {
    path: "/landing",
    component: Landing,
  },
  {
    path: "/profile",
    component: Profile,
  },
  {
    path: "/",
    component: Index,
  },
  { path: "/:pathMatch(.*)*", redirect: "/" },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

createApp(App).use(router).mount("#app");
